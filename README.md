Script de nOOb en perl6
=======================

### Why ? ###

These script files are made for learn perl6 a little and try things with it.


### What is this repository for? ###

The script normally would be able to just create a yaml file for put organised repos for Vim plugins, then install/remove/update them.


#### The design pattern ###

The design pattern is easy:

* objects/Datas.pm6 is for a class for do things from yaml file repositories (this is the class object for yaml content)
* db/Add.pm6 is for add actions of repos inside  yaml file
* db/Remove.pm6 for remove actions of repos inside yaml file
* db/Modify.pm6 for modify group names, repos names or urls inside yaml file
* repos/Install.pm6 for install actions in the vim bundle directory
* repos/Uninstall.pm6 for uninstall actions in the vim bundle directory
* repos/Update.pm6 for update plugin s repos inside the vim bundle directory
* check/Check.pm6 file is for actions on check things and list repos or installed plugins of vim
* VimPlugger.pl6 is the main file script.

### How do I get set up working ? ###

* clone this repo somewhere you like
* install rakudo for make running perl6 (can see there if you want to install by manualy: [rakudo][3])
* install zef for be able to easy install modules (look at this page for manual install: [zef][4])
if same me your packages are not updateed, getter to use git clone from github repos own pages (for rakudo and zef), then copy libs and binaries or add PATH env variable the direction where you binaries are.
* check you can run perl6 from there
* check your ncurses libs are not "magic linked", if they are (archlinux does, redhat does too...), you have to export a variable env PERL6_NCURSES_LIB to your real final ncurses lib is (for me on archlinux):
`export PERL6_NCURSES_LIB=/usr/lib/ncursesw.so.6.0`
then add this command line to your .zshrc file or .bashrc file to get this variable env allways present after log off/in 
* install module Prompt::Gruff (from zef, look here for more info about Prompt:Gruff: [Prompt::Gruff][7]) 
* install module Term::Choose, this one use Ncruses lib, so take care you have it or install ncruses libs and check what i told you before... (from zef and you can also look at this: [Term::Choose][6])
* install module Git::Wrapper (now Git::Wrapper is from nicqrocks github repo and works fine, look there: [Git::Wrapper][5])
* install a special YAML module from there: [YAML][2], you have to git clone this remote repo then go in the directory cloned and apply
`zef install .`

### Who do I talk to? ###

* I'm actively ask for Dudes on freenode #perl6 (very sympatics and patients)
* I'm looking for official perl6 doc on [perl6 official site][1]

[1]: https://docs.perl6.org/                              "perl6 official site"
[2]: https://github.com/yaml/yaml-perl6                   "YAML"
[3]: https://github.com/rakudo/rakudo/                    "rakudo"
[4]: https://github.com/ugexe/zef                         "zef"
[5]: https://github.com/nicqrocks/p6-Git-Wrapper          "Git::Wrapper"
[6]: https://github.com/kuerbis/Term-Choose               "Term::Choose"
[7]: https://github.com/adaptiveoptics/P6-Prompt-Gruff    "Prompt::Gruff"
