#---Uninstall.pm---
unit module Uninstall;
use Prompt::Gruff::Export;
use Check;
use Datas;

my %q = "select_groups" => "Repos of wich group do you want to uninstall ?",
        "select_group"  => "Choose a group from where you will choose repo to uninstall: ",
        "select_repos"  => "Choose some repos to uninstall: ";


###### PUBLIC FUNCTIONS ###################################################################

our sub some_repos_from($yaml) {
  my $group_name = Check::choose_group($yaml, ~%q<select_group>);
  my @repos_titles = Check::choose_repos_from($yaml, $group_name, ~%q<select_repos>);
  uninstall_repos_list($yaml, $group_name, @repos_titles);
}

our sub all_from_groups($yaml) {
  my @groups = Check::choose_groups($yaml, ~%q<select_group>);
  @groups.map: { uninstall_repos_of_group($yaml, *<group>) } ;
}

our sub all_repos($yaml) {
  for $yaml.datas -> %group { uninstall_repos_of_group($yaml, %group<group>); } 
}

###### PRIVATE FUNCTIONS ##################################################################

my sub uninstall_from_url($yaml, Str $url is required) {
  my $path = $yaml.repo_dir ~~ "/" ~~ Check::give_me_dir_from_url($url);
  run 'rm -Rf', "$path";  
}

my sub uninstall_repos_of_group($yaml, Str $group_name is required) {
  my @url = $yaml.repo_objects($group_name, "url");
  @url.map: uninstall_from_url($yaml, $_);
} 

my sub uninstall_repos_list($yaml, $group, @repos_titles is required) {
  @repos_titles.map: { $yaml.remove_repo($group, $_); } 
}
