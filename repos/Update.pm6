#---Update.pm---
unit module Update;
use Prompt::Gruff::Export;
use Git::Wrapper;
use Check;
use Datas;

my @q = "select_groups" => "Repos of wich group do you want to update ?";

###### PUBLIC FUNCTIONS ##############################################################

our sub some_repos_from($yaml) {

}

our sub all_from_groups($yaml) {

}

our sub all_repos($yaml) {

}

###### PRIVATE FUNCTIONS #############################################################

my sub update_repos_of_group($yaml, Str $group_name is required) {

}

my sub git_pull_repo(Str $repo_url is required) {

}
