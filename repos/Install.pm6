#---Install.pm---
unit module Install;
use Prompt::Gruff::Export;
use Git::Wrapper;
use Check;
use Datas;

my %q = "select_groups" => "Select some groups of repos you want to install ?",
        "select_repos"  => "Select some repos to install: ",
        "select_group"  => "Choose a group from where you will install some repos: ";


###### PUBLIC FUNCTIONS ##################################################################

our sub some_from_group($yaml) {
  my $group_name = Check::choose_group($yaml, ~%q<select_group>);
  my @repo_titles = Check::choose_repos_from($yaml, $group_name, ~%q<select_repos>);
  my %failed;
  @repo_titles.map: { install_repo($yaml, $group_name, $_, %failed) };
  return Check::show_errors(%failed);
}

our sub all_from_groups($yaml) {
  my @groups_names = Check::choose_groups($yaml, ~%q<select_groups>);
  my $err_flag = 0;
  my %failed;
  @groups_names.map: { install_repos_from($yaml, $_, %failed) };
  return Check::show_errors(%failed);
}

our sub all_repos($yaml) {
  my %failed;
  my @groups = $yaml.groups();
  for @groups -> %group {
    for %group<repos> -> %repo { 
      install_repo($yaml, %group<group>, %repo<url>, %failed); 
    }
  }
  return Check::show_errors(%failed);
}

##### PRIVATE FUNCTIONS ##################################################################

my sub install_repos_from($yaml, Str $group_name is required,
                                 %failed is required) {
  my @answer;
  my $git = Git::Wrapper.new(gitdir => $yaml.repo_dir);
  my %group := $yaml.group($group_name);
  for %group<repos> -> %repo { @answer = $git.run: 'clone', %repo<url>; } 
}

my sub install_repo($yaml, Str $group_name is required, 
                           Str $title is required,
                           %failed is required) {
  my $url = $yaml.url_of($group_name, $title);
  say "my url is: $url";
  my $git = Git::Wrapper.new(gitdir => $yaml.repo_dir);
  my @answer = $git.run: "clone", $url;
  my @logs = $git.log;
  say "LOGs => " ~ @logs ~ "\nAnswer: " ~ @answer;
}


