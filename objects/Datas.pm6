#--datas.pm---

unit class Datas;
use YAML;

has Str $.filename is rw;
has $.datas;
has Str $!directory= %*ENV<HOME> ~ "/.vim/installer";
has Str $.repo_dir is readonly= %*ENV<HOME> ~ "/.vim/bundle";
has Str $!full_filename= $!directory ~ "/$!filename";
method new (Str $filename) {
  return self.bless: filename => $filename;
}

method TWEAK {
  self!initialize_directory();
  self!initialize_filename_data();
}

method !initialize_directory() {
  unless $!directory.IO.d  { mkdir $!directory or die "unable to create directory"; }
}

method !initialize_filename_data() {
  unless ($!full_filename.IO.f)  { 
    my $default = $[ ${ group => "default", repos => [ ] } ];
    spurt $!full_filename, yaml.dump( $default );
  }
  $!datas = yaml.load(slurp $!full_filename);
}

method save {
  spurt $!full_filename, yaml.dump($!datas) or die "can not save datas.yml";
  say "Datas has been saved.";
}

method add_group( Str $group_name is required ){
  $!datas.push: ${ group => "$group_name", repos => [] };
}

method add_repo(Str $group_name is required, 
                Str $title      is required, 
                Str $url        is required) {
  $!datas.map: {
    $_<repos>.push: { title => "$title", url => "$url"} if $_<group> eq $group_name; }
}

method remove_repo(Str $group_name is required,
                   Str $title is required) {
  $!datas.map: {
    next unless .<group> eq $group_name;
    .<repos> .= grep: *<title> ne $title; 
  }
  $.save;
}  

method remove_group(Str $group_name is required) {
  $!datas .= grep: { .<group> ne $group_name };
  $.save;
}

method group(Str $group is required) {
  my %group := $_ with $!datas.first: *<group> eq $group;
  return %group;
}

method groups() {
  my @groups;
  $!datas.map: { @groups.push: $_<group>; }
  return @groups;
}

method repos(Str $group is required) {
  my @repos := $_<repos> with $!datas.first: *<group> eq $group;
  return @repos;
}

method repos_object(Str $group is required, Str $key = "title") {
  my @repos := $.repos($group);
  my @repo_objects;
  @repos.map: { @repo_objects.push: ~$_{$key} };
  return @repo_objects;
}

method url_of(Str $group_name is required, Str $title is required) {
  my %group := $.group($group_name);
  my $url := $_<url> with %group<repos>.first: *<title> eq $title;
  return $url;
}

method rename_group(Str $group_old, Str $group_new) {
  $!datas.map: {
    next unless .<group> eq $group_old;
    .<group> = $group_new;
    }
  $.save;
}

method rename_title_repo(Str $group, Str $repo_title_old, Str $repo_title_new) {
  my %group := $.group($group);
  for %group<repos>.first: *<title> eq $repo_title_old {
    $_<title> = $repo_title_new;
  }
  $.save;
}

method rename_url_repo(Str $group, Str $repo_url_old, Str $repo_url_new) {
  my %group := $.group($group);
  for %group<repos>.first: *<url> eq $repo_url_old {
    $_<url> = $repo_url_new;
  }
  $.save;
}
