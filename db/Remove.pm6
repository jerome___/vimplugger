#---Remove.pm----
unit module Remove;
use Term::Choose;
use Prompt::Gruff::Export;
use Check; 
use Datas;

my %q = "choose_group_to_remove"   => 
                      "Choose a group you would like to remove from yaml file",
        "choose_group"             => 
                      "Chosse a group from where some repos has to be removed",
        "select_repos"             => 
                      "Choose any repos you want to remove from yaml file";

###### PUBLIC FUNCTIONS #############################################################

our sub group($yaml) {
  my $group_name = Check::choose_group($yaml, ~%q<choose_group_to_remove>) 
                    until defined $group_name ;
  $yaml.remove_group($group_name);
}

our sub repos($yaml) {
  say "remove repos";
  my $group_name = Check::choose_group($yaml, ~%q<choose_group>) 
                    until defined $group_name ;
  my @repo_titles = Check::choose_repos_from($yaml, $group_name, ~%q<select_repos>);
  @repo_titles.map: { $yaml.remove_repo($group_name, $_) };
}
