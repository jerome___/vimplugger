#---Modify.pm----
unit module Modify;
use Term::Choose;
use Prompt::Gruff::Export;
use Check; 
use Datas;

my %q = 
  change_title   => "Change the title : ",
  change_url     => "Change the url   : ",
  change_group   => "Change the group name: ",
  select_group   => "Select the group from where you want to change repo",
  which_group    => "Which group do you want to change the name ?",
  title_url      => "Do you want to modify the title, or the url ? [t/u]",
  an_other       => "Would you like to modify an other one repo ? ",
  select_repo    => "Select the repo you would like to modify ";

###### PUBLIC FUNCTIONS #############################################################

our sub repos($yaml) {
  my $group_name = Check::choose_group($yaml, ~%q<select_group>);
  my $repo_title = Check::choose_repo_from($yaml, $group_name, ~%q<select_repo>);
  my $choice = choose(["title", "url"], :prompt(~%q<title_url>), 
                        :index(1), :mouse(1), :layout(2));
  given $choice {
    when 0 { modify_repo_title($yaml,  $repo_title); }
    when 1 { modify_repo_url($yaml, $repo_title); }
  } 
  my %failed;
  return Check::show_errors(%failed);
}

our sub group($yaml) {
  my $group_name = Check::choose_group($yaml, ~%q<which_group>);
  modify_group_name($yaml, $group_name);
}

our sub repo_title($yaml) {
  my $group_name = Check::choose_group($yaml, ~%q<select_group>);
  my $repo = Check::choose_repo_from($yaml, $group_name, ~%q<select_repo>);
  modify_repo_title($yaml, $group_name, $repo<title>);
}

our sub repo_url($yaml) {
  my $group_name = Check::choose_group($yaml, ~%q<select_group>);
  my $repo = Check::choose_repo_from($yaml, $group_name, ~%q<select_repo>);
  modify_repo_url($yaml, $group_name, $repo<url>);
}

###### PRIVATE FUNCTIONS ############################################################

my sub modify_group_name($yaml, Str $group_name is required) {
  my $new_name =  prompt-for( ~%q<change_group> );
  $yaml.rename_group( $group_name, $new_name);

}

my sub modify_repo_title($yaml, Str $group, Str $old_title is required) {
  my $new_title =  prompt-for( ~%q<change_title> );
  $yaml.rename_title_repo($group, $old_title, $new_title)
}

my sub modify_repo_url($yaml, Str $group, Str $old_url is required) {
  my $new_url = prompt-for( ~%q<change_url> );
  $yaml.rename_url_repo($group, $old_url, $new_url);
}
