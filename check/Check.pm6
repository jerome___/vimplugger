#---Check.pm---
unit module Check;
use Prompt::Gruff::Export;
use Term::Choose;
use Git::Wrapper;
use Datas;

###### PUBLIC FUNCTIONS ###################################################################

our sub group_exist($yaml, $group is required) {
  return defined $yaml.datas.first: *<group> eq $group;
}

our sub repo_is_correct($yaml, Str $group_name is required, 
                               Str $title is required, 
                               Str $url is required) {
  return "title exist" if object_repo_exist( $yaml, "title", $title, $group_name );
  return "url exist" if object_repo_exist( $yaml, "url", $url, $group_name );
  return "remote repo failed" unless git_repo_exist ( $url );
  return "want to change" unless prompt-for( 
         "title: $title\nurl: $url\ncorrect ? Do we save this ?", :yn(True) ); 
  return "is empty" if ($url eq "" || $title eq "");
  return "all is ok";
}

our sub repos_of_group_exist($yaml, Str $group is required, 
                                    @repos_list is required) {
  my @repos_failed;
  my @repos = $yaml.repos($group);
  for @repos -> %repo { @repos_failed.push(%repo) if git_repo_exist(%repo<url>); }
  return @repos_failed;
}

our sub choose_groups($yaml, Str $speech = "Choose one or more group(s): ") {
  my @group_list = $yaml.groups();;
  my @group_names = choose-multi( @group_list, :prompt("$speech"),
                                :index(0), :mouse(1), :layout(2) );
  return @group_names;
}

our sub choose_group($yaml, Str $speech = "Choose a group: ") {
  my @group_list = $yaml.groups();
  my $group_name= choose( @group_list, :prompt("$speech"),
                         :index(0), :mouse(1), :layout(2) );
  return $group_name;
}

our sub choose_repos_from($yaml, Str $group, Str $speech = "Choose some repos: ") {
  my @repos_titles = $yaml.repos_object($group);
  my @repos = choose-multi( @repos_titles, :prompt("$speech"), 
                            :index(0), :mouse(1), :layout(2) );
  return @repos;  
}

our sub choose_repo_from($yaml, Str $group_name, Str $speech) {
  my @repos_list = $yaml.repos($group_name);
  my $repo = choose(@repos_list, :prompt($speech), :index(0), :mouse(1), :layout(2));
  return $repo;
}

our sub show_errors(%failed is required) {
  if %failed.elems != 0 { say %failed; return False; }
  return True;
}

our sub list_db_all($yaml) {
  say "List of all repos inside your yaml file: $yaml.filename\n";
  my @groups = $yaml.groups();
  (1..100).map: { say "" };
  for @groups -> $group { list_db_group($yaml, $group); }
  prompt-for( "hit key for go back to the initial menu...", :required(False));
}

our sub list_db_from($yaml) {
  my $group_name = choose_group($yaml, "Which group you want to see list of repos from ?");
  list_db_group($yaml, $group_name);
  prompt-for( "hit key for go back to the initial menu...", :required(False));
}

our sub list_all_installed($yaml) {
  prompt-for( "hit key for go back to the initial menu...", :required(False));
}

our sub give_me_dir_from_url($yaml, $url) {
  my @temp = $url.split: "/";
  my $dir := @temp[*-1] ~~ /\w+/;
  return $dir;
}

###### PRIVATE FUNCTIONS ##################################################################

my sub object_repo_exist($yaml, Str $object, 
                                Str $object_name, 
                                Str $group_name) { # object=title|url 
  my %group = $yaml.group($group_name);
  return defined %group<repos>.first: *.{$object} eq $object_name;
}

my sub git_repo_exist(Str $url is required) {
  my ( $stderr, $stdout );
  my $git = Git::Wrapper.new(gitdir => '.');
  my @answer = $git.run: 'ls-remote', $url;
  return: @answer[*-1] !eq "";
}

my sub list_db_group($yaml, Str $group is required) {
  say "$group :\n";
  my @repos = $yaml.repos($group);
  for @repos -> %repo {
    my $title = %repo<title>;
    my $url = %repo<url>;
    say "\t$title:\t\t$url\n";
  }
}
