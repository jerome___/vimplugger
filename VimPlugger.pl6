#!/usr/bin/perl6

use lib "repos/";
use lib "db/";
use lib "check/";
use lib "objects/";
use Term::Choose;
use Add;
use Remove;
use Modify;
use Install;
use Uninstall;
use Update;
use Check;
use Datas;

my $mode;
my $yaml = Datas.new("my_pl6.yml");
my %menu = ( Initial   => < List Add Modify Remove Install Uninstall Update Quit >,
             List      => ( "List repos of group", "List all the repos of database",
                            "List all installed repos" ),
             Add       => ( "Add new group", "Add new repos to group" ),
             Modify    => ( "Modify name of group", "Mofify repos of a group",
                            "Modify a repo title", "Modify a repo url" ),
             Remove    => ( "Remove group", "Remove repos from a group" ),
             Install   => ( "Install all repos from groups",
                            "Install some repos from a group", "Install all repos" ),
             Uninstall => ( "Uninstall repos from group", "Uninstall all repos" ),
             Update    => ( "Update all the repos from a group", "Update all the repos" ) );

# Questions: what to do ?
my sub menu($title, @list) {
   my $title_ex = "\nWhat do you want to do ?\n $title";
    $mode = choose( @list, :index(0), :layout(2), :mouse(1), :prompt($title_ex) );
  return $mode;
}

while ( $mode = menu("Choose:", %menu<Initial>) ) {
  given $mode {
    when "List" {
      my $action = menu("Print a list: ", %menu<List>);
      given $action {
        when "List all the repos of database"       { Check::list_db_all($yaml); }
        when "List repos of group"                  { Check::list_db_from($yaml) }
        when "List all installed repos"             { Check::list_all_installed($yaml); } } }
    when "Add" {
      my $action = menu("Add something: ", %menu<Add>);
      given $action {
        when "Add new group"                        { Add::group($yaml); }
        when "Add new repos to group"               { Add::repos($yaml); } } }
    when "Modify" {
      my $action = menu("Modification :", %menu<Modify>);
      given $action {
        when "Modify a repo url"                    { Modify::repo_url($yaml); }
        when "Modify a repo title"                  { Modify::repo_title($yaml); }
        when "Modify a repos from a group"          { Modify::repos($yaml); }
        when "Modify name of group"                 { Modify::group($yaml); } } }
    when "Remove" {
      my $action = menu("Remove:", %menu<Remove>);
      given $action {
        when "Remove repos from a group"            { Remove::repos($yaml); }
        when "Remove group"                         { Remove::group($yaml); } } }
    when "Install" {
      my $action = menu("Install for vim", %menu<Install>);
      given $action {
        when "Install some repos from a group"      { Install::some_from_group($yaml); }
        when "Install all repos from groups"        { Install::all_from_groups($yaml); }
        when "Install all repos"                    { Install::all_repos($yaml) } } }
    when "Uninstall" {
      my $action = menu("Uninstall repos:", %menu<Uninstall>);
      given $action {
        when "Uninstall some repos from a group"    { Uninstall::some_from_group($yaml); }
        when "Uninstall all repos from groups"      { Uninstall::all_from_groups($yaml); }
        when "Uninstall all repos"                  { Uninstall::all_repos($yaml); } } }
    when "Update" {  }
    when "Quit" { exit; }
  }
}
